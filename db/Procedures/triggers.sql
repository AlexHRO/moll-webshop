
/*
--after_tbl_orders_insert--

Trigger activated upon inserting a new order in tb_orders

It creates two rows, one for tbl_orderstatus and one for tbl_orderhistory.
This is the initial state of the order, whent the customer has just finalized their order specifications.
Chronologically, the next step would be for them to choose a payment method. 

*/

DELIMITER ||
CREATE TRIGGER after_tbl_orders_insert 
AFTER INSERT ON tbl_orders
FOR EACH ROW

BEGIN
	INSERT INTO tbl_orderstatus (fld_OrderId, fld_targetDeliveryDate, fld_dateUpdated)
    VALUES(NEW.fld_OrderId, DATE_ADD(NOW(), INTERVAL 1 WEEK), NOW());
    
	INSERT INTO tbl_orderhistory (fld_OrderId, fld_orderStatus, fld_LastAction, fld_ActionDate)
    VALUES(New.fld_OrderId, 'Pending', 'In payment', Now());

END ||

DROP TRIGGER after_tbl_orders_insert;
