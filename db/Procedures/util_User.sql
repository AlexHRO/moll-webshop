DELIMITER ||
CREATE PROCEDURE `FindUserById` (IN in_givenUserId int(11))
BEGIN
	SELECT * 
    FROM tbl_userData ud
    WHERE ud.fld_UserId = in_givenUserId;
END||

DROP PROCEDURE FindUserById;


DELIMITER ||
CREATE PROCEDURE `FindUserNameById` (IN in_givenUserId int(11))
BEGIN
	CALL FindUserById(in_givenUserId);
    SELECT fld_UserName FROM temp_table;
END
||

DROP PROCEDURE FindUserNameById;

DELIMITER ||
CREATE PROCEDURE `user_CheckIfExistsById` (IN in_userId INT(11), OUT out_result BOOLEAN)
BEGIN
	SET out_result = EXISTS(SELECT fld_userid
						  FROM tbl_userdata ud
						  WHERE ud.fld_userid = in_userid);
END
||

DROP PROCEDURE user_CheckIfExistsById;

DELIMITER ||
CREATE PROCEDURE `CheckIfUserNameIsTaken` (IN in_userName VARCHAR(25), OUT out_result INT)
BEGIN
	SET out_result = EXISTS( SELECT fld_UserName
							FROM tbl_userdata ud
							WHERE ud.fld_UserName = in_userName);
END
||

DROP PROCEDURE CheckIfUserNameIsTaken;

DELIMITER ||
CREATE PROCEDURE `FindUserByEmail` (IN in_email VARCHAR(50))
BEGIN
	SELECT *
    FROM tbl_userData ud
    WHERE ud.fld_Email = in_email;
END
||

DROP PROCEDURE FindUserByEmail;

-- Verifies a user and activates their account. This procedure is called after clicking the link in the email the user receives
DELIMITER ||
CREATE PROCEDURE `user_Verify`(IN in_token VARCHAR(25), IN in_userid INT(11), OUT out_result INT(1))
BEGIN
	UPDATE tbl_userdata ud
    SET ud.fld_isActivated = 1
    WHERE ud.fld_UserId = in_userId AND ud.fld_activationCode = in_token;
    
    SET out_result = ( SELECT fld_isActivated
				   FROM tbl_userdata ud
				   WHERE ud.fld_UserId = in_userid);
END
||

DROP PROCEDURE user_Verify;

-- Returns a users ID 
DELIMITER ||
CREATE PROCEDURE `user_FindUserIdByEmail`(IN in_email VARCHAR(50), OUT out_userId INT(11))
BEGIN
	SET out_userId = (SELECT fld_UserId
					FROM tbl_userdata ud
					WHERE ud.fld_Email = in_email);
END
||

DROP PROCEDURE user_FindUserIdByEmail;

-- Registers a user in our DB
DELIMITER ||
CREATE PROCEDURE `user_Register`(IN in_userName VARCHAR(25), IN in_password VARCHAR(50), IN in_firstName VARCHAR(25), IN in_lastName VARCHAR(25), IN in_gender VARCHAR(6), IN in_address VARCHAR(75), IN in_zipCode VARCHAR(10), IN in_dob VARCHAR(10), IN in_phoneNumber VARCHAR(20), IN in_emailAddress VARCHAR(50), IN in_activationCode VARCHAR(12), IN in_isActivated tinyInt(1), OUT out_userId INT(11))
BEGIN
	INSERT INTO tbl_userdata (fld_UserName,fld_Password,fld_FirstName,fld_LastName,fld_Gender,fld_Address,fld_ZipCode,fld_DateOfBirth,fld_PhoneNumber,fld_Email, fld_activationCode, fld_isActivated, fld_adminPriv)
    VALUES (in_username, in_password, in_firstName, in_lastName, in_gender, in_address, in_zipCode, in_dob, in_phonenumber, in_emailAddress, in_activationCode, in_isactivated, 'N');

	SET out_userId = (SELECT fld_UserId FROM tbl_userdata WHERE fld_email = in_emailAddress);
END
||

DROP PROCEDURE user_register;




DELIMITER ||
CREATE PROCEDURE `getAllUsers`()
BEGIN
	SELECT *
    FROM tbl_userdata;
END;
||