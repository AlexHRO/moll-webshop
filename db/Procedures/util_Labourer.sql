DELIMITER ||
CREATE PROCEDURE `FindLabourerById` (IN in_givenLabourerId VARCHAR(11))
BEGIN
	SELECT *
    FROM tbl_labourerdata ld
    WHERE ld.fld_LabourerId = in_givenLabourerId;
END
||

DROP PROCEDURE FindLabourerById;
