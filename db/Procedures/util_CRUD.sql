/*
--util_AlterVarCharField--

Generic Procedure for altering a value of a data row in a table of choosing.
Creates a Prepared Statement by concatenating String variables into an Update statement

--Parameters--

TargetTable VARCHAR(100), 	The table you want to change a value in

FieldToChange VARCHAR(100),	The column you want to change a value in

PrimaryKeyColumn VARCHAR(100), The Primary key column of the table you want to change a value in

ValueForField VARCHAR(100),	The value you want to give to the column
            
            
*/

DROP PROCEDURE `util_UpdateVarCharField`;

DELIMITER ||
CREATE PROCEDURE `util_UpdateVarCharField`(IN in_TargetTable VARCHAR(100), 
										  IN in_FieldToChange VARCHAR(100), 
										  IN in_PrimaryKeyColumn VARCHAR(100), 
                                          IN in_ValueForField VARCHAR(10000),  
                                          IN in_Id INT(11)
                                          )
BEGIN

	SET @PreparedStatement = CONCAT('UPDATE ', in_TargetTable,
									' SET ', in_FieldToChange, ' = ', '''', in_ValueForField, ''' ',
									'WHERE ', in_PrimaryKeyColumn,' = ', in_Id, ';');
	
		PREPARE statement FROM @PreparedStatement;
		EXECUTE statement; 
		
		DEALLOCATE PREPARE statement;
END
||


DROP PROCEDURE `util_UpdateNumberField`;
DELIMITER ||
CREATE PROCEDURE `util_UpdateNumberField`(IN in_TargetTable VARCHAR(100), 
										  IN in_FieldToChange VARCHAR(100), 
										  IN in_PrimaryKeyColumn VARCHAR(100), 
                                          IN in_ValueForField DOUBLE,  
                                          IN in_Id INT(11))
BEGIN
	SET @PreparedStatement = CONCAT('UPDATE ', in_TargetTable,
									' SET ', in_FieldToChange, ' = ', in_ValueForField,
									' WHERE ', in_PrimaryKeyColumn,' = ', in_Id, ';');
	
		PREPARE statement FROM @PreparedStatement;
		EXECUTE statement; 
		
		DEALLOCATE PREPARE statement;
END
||


/*
--util_DeleteRow--

Generic Procedure for deleting a row.
Does not work for rows with composite keys. 

--Parameters--

in_TargetTable VARCHAR(100), 	The table you want to delete a row from

in_TargetColumn VARCHAR(100),	The primary key column

in_TargetValue INT,	The value of the primary key 
            
            
*/

DROP PROCEDURE `util_DeleteRow`;
DELIMITER ||
CREATE PROCEDURE `util_DeleteRow`(IN in_TargetTable VARCHAR(100),
								  IN in_TargetColumn VARCHAR(100),
								  IN in_TargetValue INT)                                  
BEGIN
  
	SET @PreparedStatement = CONCAT('DELETE FROM ', in_TargetTable,
									' WHERE ', in_TargetColumn, ' = ' '''', in_TargetValue, '''');
                                    
	PREPARE statement FROM @PreparedStatement;
    EXECUTE statement;
END
||



/*
--util_CreateRow

Generic procedure for inserting a Row in a table

Some tables have primary keys that insert themselves using Auto increment or sequences. In that case,
you could just create an empty row and populate its fields with the UpdateProcedures above.

However, not all tables have self-inserting Primary or Foreign Keys, so inserting an initial value
is neccessary. This procedure also allows for that.

Example of calls:

CALL util_CreateRow('tbl_orders' , 'fld_OfferedServiceId, fld_email', '9, ''test@mail.com'' ');

^ Both the columns and their respective values must be given as strings. Values that are text-based,
such as VarChar must be surrounded by quotes (double quotes to escape the character)
*/

DROP PROCEDURE `util_CreateRow`;

DELIMITER ||
CREATE PROCEDURE `util_CreateRow` (IN in_TargetTable VARCHAR(100),
									IN in_ColumnString VARCHAR(1000),
									IN in_ValueString VARCHAR(1000),
                                    OUT out_generatedId INT(11))
BEGIN
	SET @PreparedStatement = CONCAT('INSERT INTO ', in_TargetTable, ' (', in_ColumnString, ') ' ,
								' VALUES( ', in_ValueString, ' );' );
								
	PREPARE statement FROM @PreparedStatement;
	EXECUTE statement;
	
	SET out_generatedId = last_insert_id();
END
||

