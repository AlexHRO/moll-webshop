DELIMITER ||
CREATE PROCEDURE `FindServiceById` (IN in_givenServiceId int(11))
BEGIN
	SELECT *
    FROM tbl_servicedata sd
    WHERE sd.fld_ServiceId = in_givenServiceId;
END
||

DROP PROCEDURE FindServiceById;

DELIMITER ||
CREATE PROCEDURE `CheckShoppingCartItemExistance` (IN in_givenServiceId int(11), IN in_givenUserId int(11), OUT out_result BOOLEAN)
BEGIN
	SET out_result = EXISTS(SELECT *
	FROM tbl_shoppingcart sc
    WHERE sc.fld_offeredserviceid = in_givenServiceId AND sc.fld_userid = in_givenUserId);
END
||

DROP PROCEDURE CheckShoppingCartItemExistance

DELIMITER ||
CREATE PROCEDURE `GetShoppingCartItems` (IN in_givenUserId int(11))
BEGIN
	SELECT fld_offeredserviceid
    FROM tbl_shoppingcart sc
    WHERE sc.fld_userid = in_givenUserId;
END
||