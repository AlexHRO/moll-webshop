/*
--Authentication--

The authentication process is divided by three procedures. One checks if the user exists inside the database,
the second one checks if the given password matches with the one inside the database, the third one combines
the two and returns a user id and user name.


*/


/*
--auth_CheckUserExists--

Checks if a user is present currently in the database by checking email addresses.

--Parameters--

in_emailAddress VARCHAR(25), the email address to be checked

result INT, the result of the check. 0 if false, 1 if true.

*/


DROP PROCEDURE auth_CheckUserExists;

DELIMITER ||
CREATE PROCEDURE `auth_CheckUserExists`(IN in_emailAddress VARCHAR(100), OUT out_result TINYINT(1))
BEGIN
	SET out_result = EXISTS (
			SELECT *
			FROM tbl_userdata tbl
			WHERE tbl.fld_Email = in_emailAddress);
				   

END
||

DROP PROCEDURE auth_CheckUserExistsLogin;

DELIMITER ||
CREATE PROCEDURE `auth_CheckUserExistsLogin`(IN in_emailAddress VARCHAR(100))
BEGIN
			SELECT *
			FROM tbl_userdata tbl
			WHERE tbl.fld_Email = in_emailAddress;

END
||


/*
--auth_CheckPasswordMatch

Checks if the combination of email address and password exist in the database.

--Parameters--

in_emailAddress VARCHAR(25), the email address to be checked

in_password VARCHAR(25), the password to be checked

UserId INT, the userId that gets returned after the check. Returns null if check failed, returns a userId if check successful

UserName VARCHAR(25), the user name that gets returned after the check.

*/

DROP PROCEDURE auth_CheckPasswordMatch;

DELIMITER ||
CREATE PROCEDURE `auth_CheckPasswordMatch`(IN in_emailAddress VARCHAR(25), IN in_password VARCHAR(25), OUT out_UserId INT, OUT out_UserName VARCHAR(25), OUT out_adminPriv CHAR)
BEGIN

	DROP TEMPORARY TABLE IF EXISTS tempResult;
    CREATE TEMPORARY TABLE tempResult AS

			SELECT fld_userid, fld_UserName, fld_AdminPriv
			FROM tbl_userdata tbl
			WHERE tbl.fld_Email = in_emailAddress AND tbl.fld_Password = in_password;
        
	SET out_UserId = (
			SELECT fld_userid
            FROM tempResult
		);
        
	SET out_UserName = (
			SELECT fld_username
            FROM tempResult
    );
    
    SET out_AdminPriv = (
			SELECT fld_AdminPriv
            FROM tempResult
    );
END
||


/*
--auth_CheckIfActivated

Checks if the user has activated their account by email

--Parameters--

p_userId INT(11), the user id

p_result TINYINT(1), the result of the check

Returns 0 if not activated, 1 if activated

*/

DROP PROCEDURE auth_checkIfActivated;

DELIMITER ||
CREATE PROCEDURE auth_checkIfActivated (IN in_userId INT(11), OUT out_result TINYINT(1) )
BEGIN
	
	SET out_result = (SELECT fld_isActivated
					FROM tbl_userdata ud
					WHERE ud.fld_UserId = in_userId );
END
||
/*
--LOGIN--

This procedure combines the previous three in order to handle authentication and return appropriate values.
If authentication is successful, both the userID and User Name are returned.
If authentication failed because the given email address is non-existant, a -1 is returned as the userId
If authentication failed because the given password is wrong, a 0 is returned as the userId.
If the user has not yet activated their account, a -3 is returned as the userId 

*/

DROP PROCEDURE auth_LOGIN;

DELIMITER ||
CREATE PROCEDURE `auth_LOGIN`(IN in_emailAddress VARCHAR(25), IN in_password VARCHAR(25), OUT out_foundUserId int, OUT out_foundUserName VARCHAR(25))
BEGIN
	DECLARE procResult BOOLEAN;
    DECLARE userId int;
    
    -- Check if account exists by searching for the email
    CALL auth_CheckUserExists(in_emailAddress, @result);
    
    SELECT @result INTO procResult;
	
    IF(procResult = 1) THEN
		
        -- The account has been found, Check if passwords match
		CALL auth_CheckPasswordMatch(in_emailAddress, in_password, @t_foundUserId, @t_foundUserName);
        
        -- Set the results
        -- If the passwords do not match, a null value is returned for t_foundUserId. In that case, we set p_foundUserId to 0 for further processing 
        SELECT IFNULL(@t_foundUserId, 0) INTO out_foundUserId;
        SELECT IFNULL(@t_foundUserName, '') INTO out_foundUserName;
        
		-- Check if User has activated their account
        CALL auth_checkIfActivated(out_foundUserId, @activationResult);
        
        IF(@activationResult = 0) THEN
        -- User has not activate their account, set p_foundUserId to -3 for further processing
			SET out_foundUserId = -3;
        END IF;
        
	END IF;	
    
    IF(procResult = 0) THEN
    
		-- Account has not been found. Set p_foundUserId to -1 for further processing
		SET out_foundUserId = -1;
        SET out_foundUserName = '';
        
    END IF;
END
||