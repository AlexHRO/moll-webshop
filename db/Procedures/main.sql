

CREATE TABLE `tbl_userdata` (
  `fld_UserId` int(11) AUTO_INCREMENT,
  `fld_UserName` varchar(25),
  `fld_Password` varchar(50),
  `fld_FirstName` varchar(25),
  `fld_LastName` varchar(25),
  `fld_Gender` varchar(6),
  `fld_Address` varchar(75),
  `fld_ZipCode` varchar(10),
  `fld_DateOfBirth` varchar(10),
  `fld_PhoneNumber` varchar(20),
  `fld_Email` varchar(50),
  `fld_activationCode` varchar(25),
  `fld_inputActivationCode` varchar(25) ,
  `fld_isActivated` tinyint(1)DEFAULT '0',
  `fld_AdminPriv` Char(1) DEFAULT 'N',
  PRIMARY KEY (`fld_UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE tbl_userdata;

ALTER TABLE tbl_userdata MODIFY COLUMN fld_isactivated int(1);
ALTER TABLE tbl_userdata DROP COLUMN fld_inputActivationCode;


UPDATE tbl_userdata SET fld_AdminPriv = 'Y' WHERE fld_UserId = 2;

CALL auth_CheckUserExists('aazoutewelle@gmail.com', @t);
SELECT @t;

CREATE TABLE tbl_orders(
	fld_OrderId int(11) AUTO_INCREMENT,
    fld_OfferedServiceId int(11),
    fld_email VARCHAR(50),
    PRIMARY KEY (fld_OrderId)
) ENGINE = InnoDB AUTO_INCREMENT = 1;

CREATE TABLE tbl_orderstatus(
	fld_OrderId int(11),
    fld_dateOrdered DATE,
    fld_targetDeliveryDate DATE,
    fld_dateDelivered DATE,
    fld_dateUpdated DATE
);

CREATE TABLE tbl_orderhistory(
	fld_OrderId int(11),
    fld_orderStatus VARCHAR(50),
    fld_lastAction VARCHAR(50),
    fld_actionDate DATE
);

INSERT INTO tbl_orderhistory (fld_OrderId, fld_orderStatus, fld_lastAction, fld_actionDate)VALUES(1, 'Delivered', 'Received product', '2019-1-31');



CREATE TABLE tbl_labourerdata(
        fld_labourerid int(11) AUTO_INCREMENT,
        fld_firstname VARCHAR(30),
        fld_lastname VARCHAR(30),
        fld_gender VARCHAR(30),
        fld_address VARCHAR(30),
        fld_zipcode VARCHAR(30),
        fld_dateofbirth DATE,
		fld_phonenumber  VARCHAR(30),
        fld_email VARCHAR(100),
        PRIMARY KEY(fld_labourerid)
)ENGINE = InnoDB AUTO_INCREMENT = 1;

CREATE TABLE tbl_servicedata (
         fld_serviceid int(11) AUTO_INCREMENT,
        fld_name VARCHAR(200),
        fld_description VARCHAR(500),
        fld_category VARCHAR(20),
        fld_imagelink VARCHAR(100),
        PRIMARY KEY (fld_serviceid)
)ENGINE = InnoDB AUTO_INCREMENT = 1;




CREATE TABLE tbl_shoppingcartitem(
	fld_userID int(11),
    fld_offeredServiceId int(11)
);

DROP TABLE tbl_offeredservicesdata;
CREATE TABLE tbl_OfferedServicesData
(
		 fld_offeredServiceId int(11) AUTO_INCREMENT,
        fld_serviceId int(11),
        fld_labourerId int(11),
        fld_addedToWishList int(11),
        fld_timesBought int(11),
        fld_cost DECIMAL( 10, 2 ),
        fld_timeFirst TIME,
        fld_timeLast TIME,
        fld_area varchar(20),
        fld_stillAvailable varchar(5),
        PRIMARY KEY(fld_offeredServiceId)
)ENGINE = InnoDB AUTO_INCREMENT = 1;



CREATE VIEW views_packages AS
	SELECT * 
	FROM tbl_labourerdata AS tl, tbl_servicedata AS ts, tbl_OfferedServicesData tosd
		INNER JOIN
        tbl_OfferedServicesData ON tl.fld_labourerid = tbl_OfferedServicesData.fld_labourerid;
/* VIEW*/


SELECT * from tbl_orders;
SELECT * FROM tbl_orderstatus;

UPDATE tbl_orderstatus SET fld_dateDelivered = ' 2019-01-25' WHERE fld_OrderId = 1;
SELECT * from tbl_orderhistory;
SELECT * from tbl_shoppingcart;
SELECT * FROM tbl_servicedata ORDER BY fld_serviceid DESC;
SELECT * FROM tbl_offeredservicesdata where fld_OfferedserviceId = 273;
SELECT * FROM tbl_labourerdata;
select * from tbl_ols where fld_OfferedServiceId = 273;
SELECT * FROM tbl_userdata;
UPDATE tbl_userdata SET fld_adminPriv = 'Y' WHERE fld_userid = 1;

TRUNCATE TABLE tbl_offeredservicesdata;
TRUNCATE TABLE tbl_orders;
TRUNCATE TABLE tbl_orderhistory;
TRUNCATE TABLE tbl_userdata;

DROP TABLE tbl_orderstatus;
DROP TABLE tbl_orderhistory;

DELETE FROM tbl_userdata WHERE fld_UserId = 28;
DROP TABLE tbl_labourerdata;
DROP PROCEDURE util_CreateRow;

CALL util_CreateRow('tbl_offeredservicesdata', 
'fld_offeredserviceid, fld_serviceid, fld_labourerid, fld_addedtowishlist, fld_timesbought, fld_cost, fld_timefirst, fld_timelast, fld_area, fld_stillavailable',
'1, 					100, 			232, 			15, 				30, 			35.62, ''10:05:00'', ''15:18:00'', ''Friesland'', ''Y'' ');

DROP VIEW tbl_ols;

CREATE VIEW tbl_ols AS
SELECT lab.fld_address, lab.fld_dateofbirth, lab.fld_email, lab.fld_firstname, lab.fld_gender, lab.fld_lastname, lab.fld_phonenumber, lab.fld_zipcode,
		serv.fld_category, serv.fld_description, serv.fld_imagelink, serv.fld_name, serv.fld_serviceid,
        off.fld_addedToWishList, off.fld_area, off.fld_cost*100 AS fld_cost, off.fld_offeredServiceId, off.fld_stillAvailable, off.fld_timeFirst, off.fld_timeLast, off.fld_timesBought
FROM tbl_labourerdata AS lab,  tbl_servicedata AS serv, tbl_offeredservicesdata AS off
WHERE lab.fld_labourerid = off.fld_labourerid AND serv.fld_serviceid = off.fld_serviceid;



CALL util_getOrderId(386, 'aazoutewelle@gmail.com', @t);
SELECT @t;

SELECT COUNT(*) from tbl_userdata;

CALL util_CreateRow('tbl_offeredservicesdata', 
'fld_offeredserviceid, fld_serviceid, fld_labourerid, fld_addedtowishlist, fld_timesbought, fld_cost, fld_timefirst, fld_timelast, fld_area, fld_stillavailable',
'1, 					100, 			232, 			15, 				30, 			35.62, ''10:05:00'', ''15:18:00'', ''Friesland'', ''Y'' ');


SELECT * FROM tbl_servicedata WHERE fld_serviceid = 105;
DELETE FROM tbl_servicedata WHERE fld_serviceid = 106;