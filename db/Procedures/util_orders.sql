/*
util_getOrders

Gets the orders linked to a specific user account
*/
DROP PROCEDURE util_GetOrders;

DELIMITER ||
CREATE PROCEDURE util_GetOrders (IN in_givenEmail VARCHAR(50))
BEGIN
	SELECT *
    FROM tbl_orders tor
    WHERE fld_email = in_givenEmail;
END
||

DROP PROCEDURE util_GetOrderHistories;

DELIMITER || 
CREATE PROCEDURE util_GetOrderHistories(IN in_orderid INT(11))
BEGIN
	SELECT *
    FROM tbl_orderhistory
    WHERE fld_OrderId = in_orderid;
END
||

DROP PROCEDURE util_GetOrderStatuses

DELIMITER ||
CREATE PROCEDURE util_GetOrderStatuses(IN in_orderid INT(11))
BEGIN
	SELECT *
	FROM tbl_orderstatus
    WHERE fld_OrderId = in_orderid;
END
||
	
DROP PROCEDURE util_GetOrderId;

DELIMITER ||
CREATE PROCEDURE util_GetOrderId (IN in_offeredserviceId INT(11), IN in_givenEmail VARCHAR(200), OUT out_result INT(11))
BEGIN
	SET out_result = (SELECT fld_orderid
    FROM tbl_orders
    WHERE fld_email = in_givenEmail AND fld_offeredserviceId = in_offeredserviceId);
END