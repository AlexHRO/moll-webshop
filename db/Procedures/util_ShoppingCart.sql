/*
	util_DeleteShoppingCartItem
    
    For now, the util_CRUD deleteRow is not compatible with tables that have composite primary keys. This procedure is
    a placeholder to do what we need with shopping cart items
*/

DROP PROCEDURE `util_DeleteShoppingCartItem`;

DELIMITER ||
CREATE PROCEDURE `util_DeleteShoppingCartItem`(
								  IN in_TargetUser INT,
								  IN in_TargetItem INT)                                  
BEGIN
  
    DELETE FROM tbl_shoppingcart
    WHERE fld_userid = in_TargetUser AND fld_offeredServiceId = in_TargetItem;
END
||
